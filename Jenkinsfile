pipeline {
    agent any

    options {
        timestamps ()
        timeout (time: 10, unit: "MINUTES")
        gitLabConnection ("gitlab")
    }

    tools {
        maven "maven"
        jdk "openjdk-8"
    }

    triggers {
        gitlab(triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: 'All')
    }

    environment {
        REPO_URL = "git@gitlab.com:Terre8055/product.git"
    }

    stages {

        stage ("Checkout") {
            steps {
                checkout scm
            }
        }

        stage ("Set Version") {
            when {
                branch "release/*"
            }
            steps {
                script {
                    sshagent(['image_pipe']){
                        sh"""
                            git fetch $REPO_URL --tags
                        """
                    }
                    env.VERSION=sh( returnStdout: true, script: "bash getTags.sh").trim()
                    println(env.VERSION)

                    sh"""
                        mvn versions:set -DnewVersion=$env.VERSION
                    """
                }
            }
        }


        stage ("Build -> Test") {
            when {
                branch "feature/*"
            }
            steps {
                script {
                    if ("${env.GIT_COMMIT_MESSAGE}".contains("#e2e")) {
                        withMaven(mavenSettingsConfig: "artifactory-settings") {
                            sh """
                                mvn package -Panalytics-e2e
                            """
                        }
                    } else {
                        withMaven(mavenSettingsConfig: "artifactory-settings") {
                            sh """
                                mvn package
                            """
                        }
                    }
                }
            }
        }



        stage ("Build -> Test -> Verify -> Publish") {
            when {
                branch "main"
            }
            steps {
                script {
                    withMaven(mavenSettingsConfig: "artifactory-settings"){
                    sh """
                        mvn deploy -DskipTests
                    """
                    }
                }
            }
        }

        stage ("Build -> Test -> Verify -> Publish -> Release") {
            when {
                branch "release/*"
            }
            
            steps {
                script {
                    withMaven(mavenSettingsConfig: "artifactory-settings"){
                    sh """
                        mvn dependency:list
                        mvn deploy -DskipTests
                    """
                    }
                }
            }
        }

        stage ("Tag") {
            when {
                branch "release/*"
            }
            steps{
                script{
                    sshagent(['image_pipe']){
                        sh"""
                            git clean -f -x
                            git tag $env.VERSION || echo "tag already exists"
                            git push --tags
                        """
                    }

                }
            }
        }
    }

    post {

        always {
            script {
                cleanWs()
            }
        }

        success {
            updateGitlabCommitStatus name: 'build', state: 'success'
        }

        failure {
            updateGitlabCommitStatus name: 'build', state: 'failed'
        }
    }
}
